package net.stanixgames.secretbox.tests;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.stanixgames.secretbox.Vertex2D;
import net.stanixgames.secretbox.ninepatch.NinePatchUtil;

public class NinePatchUtilTest
{

	@Test()
	public void verticesShouldBeNotNull()
	{
		NinePatchUtil npu = new NinePatchUtil() ;
		Vertex2D[][] vertices = npu.parseVertices("D:\\test2.9.png") ;
		
		for(int i=0; i<vertices.length; i++)
			assertNotNull(vertices[i][0]) ;
	}

	@Test()
	public void verticesCoordinateShouldBeNotZero()
	{
		NinePatchUtil npu = new NinePatchUtil() ;
		Vertex2D[][] vertices = npu.parseVertices("D:\\test2.9.png") ;
		
		for(int i=0; i<vertices.length; i++)
			for(int j=0; j<vertices[0].length; j++)
				if((i > 0 && j != 0) || (j > 0 && i != 0))
					assertNotEquals(vertices[i][j], 0) ;
	}
}
