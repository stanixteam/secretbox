package net.stanixgames.secretbox.print;

import java.util.logging.Logger;

/**
 * Utility Class for printing a lot of stuff.
 * @author St1nger13
 * date: 27.02.2016
 */
public class PrintUtil
{
	private static final Logger logger = Logger.getLogger(PrintUtil.class.getName()) ;
	
	public static void print3DArray(float[][][] array)
	{
		StringBuilder sb = new StringBuilder() ;
		for(int i=0; i<array.length; i++)
		{
			sb.append("{  ") ;
			for(int j=0; j<array[0].length; j++)
			{
				sb.append("{") ;
				for(int z=0; z<array[0][0].length; z++)
				{
					sb.append(String.format("%4s,", array[i][j][z])) ;
					sb.append(" ") ;
				}
				sb.append("}, ") ;
			}
			sb.append("}, \n") ;
		}
		
		System.out.println(sb.toString());
	}
}
