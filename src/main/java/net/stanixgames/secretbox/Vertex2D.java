package net.stanixgames.secretbox;

public class Vertex2D
{
	public float x ;
	public float y ;
	
	public Vertex2D(float x, float y)
	{
		this.x = x ;
		this.y = y ;
	}
}
