package net.stanixgames.secretbox.ninepatch;

import net.stanixgames.secretbox.Vertex2D;

public class NinePatchData
{
	private Vertex2D[][] vertices ;
	private String texturePath ;
	
	
	public NinePatchData(String path, Vertex2D[][] vertices)
	{
		this.texturePath = path ;
		this.vertices = vertices ;
	}

	public Vertex2D[][] getVertices()
	{
		return vertices;
	}

	public String getTexturePath()
	{
		return texturePath;
	}
}
