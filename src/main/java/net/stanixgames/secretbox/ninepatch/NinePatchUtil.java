package net.stanixgames.secretbox.ninepatch;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.stanixgames.secretbox.Vertex2D;

/** 
 * Utility for work with NinePatch Textures
 * @author St1nger13
 * date: 26.02.2016
 */
public final class NinePatchUtil
{
	private static final Logger logger = Logger.getLogger(NinePatchUtil.class.getName()) ;
	/**
	 * Method read the PNG image and represents key-point like 
	 * vertices. 
	 * @param path to image
	 * @return float[][][] array of vertices
	 */
	public static Vertex2D[][] parseVertices(String path)
	{
		try
		{
			BufferedImage img = ImageIO.read(new File(path)) ;
			Vertex2D[][] vertices = readImage(img) ;
			
			logger.info("Parsed vertices from texture \"" +path+ "\". (" +vertices.length*vertices[0].length+ " vertices)") ;
			
			return vertices ;
		}
		catch(Exception e)
		{
			e.printStackTrace() ;
		}
		
		return null ;
	}
	
	public static boolean saveData(NinePatchData data, String path)
	{
		if(data != null && path != null)
		{
			Gson gson = new Gson() ;
			
			String json = gson.toJson(data) ;
			
			try(PrintWriter out = new PrintWriter(path))
			{
			    out.println(json) ;
			    logger.info("Saved Json data to \"" +path+ "\".") ;
			    
			    return true ;
			} 
			catch (FileNotFoundException e)
			{
				logger.log(Level.SEVERE, "Can't save Json data to \"" +path+ "\"!") ;
			}
		}
		
		return false ;
	}
	
/*	public float[][][] compactVertices(float[][][] vertices, boolean isFirstPixelBlack)
	{	
		float[] cut = new float[]{0, 0} ;
		float[] rz = new float[]{0, 0} ;
		
		for(int i=0; i<vertices.length; i++)
		{
			cut[0] = 0 ;
			rz[0] = 0 ;
			
			if((isFirstPixelBlack ^ (i % 2 == 0)) && i > 0 && i < vertices.length-1)
			{
				rz[0] = vertices[i][0][0] - vertices[i-1][0][0] - 1 ;
				cut[0] += rz[0] ;
			}
			
			for(int j=0; j<vertices[0].length; j++)
			{			
				if((isFirstPixelBlack ^ (i % 2 == 0)) && i > 0 && i < vertices.length)
				{	
					vertices[i][j][0] = vertices[i-1][j][0] + 1 ;	
					if(i != vertices.length-1)
						vertices[i+1][j][0] -= cut[0] ;
				}
			}
		}
		
		for(int j=0; j<vertices[0].length; j++)
		{	
			if((isFirstPixelBlack ^ (j % 2 == 0)) && j > 0 && j < vertices[0].length+1)
			{
				for(int i=0; i<vertices.length; i++)
				{
					cut[1] = 0 ;
					rz[1] = 0 ;
					if(j < vertices[0].length-1)
					{
						rz[1] = vertices[i][j][1] - vertices[i][j-1][1] - 1 ;
						cut[1] += rz[1] ;
					}
					
					vertices[i][j][1] = vertices[i][j-1][1] + 1 ;
					if(j < vertices[0].length-1)
						vertices[i][j+1][1] -= cut[1] ;
				}
			}
		}
		
		return vertices ;
	}*/
	
	public static boolean isFirstPixelBlack(Vertex2D[][] vertices)
	{
		if(vertices[0][0].x == -1)
		{
			vertices[0][0].x = 0 ;
			return true ;
		}
		
		return false ;
	}
	
	
	public static void packImage(String path) throws IOException
	{
		try
		{
			System.out.println("NinePatchPacker: Opening image...") ;
			BufferedImage img = ImageIO.read(new File(path)) ;
			
			int alpha = (img.getRGB(0, 0) >> 24) & 0xFF ;
			//int currentPixel ;
			boolean isFirstPixelBlack = false ;
			boolean isPrevPixelBlack = false ;
			boolean isCurrentPixelBlack = false ;
			
			/*** Check first pixel is it valid ***/
			
			if(alpha > 13 && alpha < 242) 
			{
				throw new IOException("NinePatchPacker: First pixel has bad transparent (" +alpha+ "/255)") ;
			}
			else
			{
				isFirstPixelBlack = (alpha < 14) ? false : (alpha > 241) ? true : false ;
				isPrevPixelBlack = isFirstPixelBlack ;
				
				logger.info("Image size [" +img.getWidth()+ " x " +img.getHeight()+ "] / " 
				+img.getWidth()*img.getHeight()+ " pixels.") ;
				logger.info("First pixel interpreted like " 
				+((!isFirstPixelBlack) ? "transparent" : "black")) ;
			}
			
			int cutY = 0 ;
			int cutX = 0 ;
			
			logger.info("Packing by Y axis...") ;
			// Goes on Y axis
			for(int j=1; j<img.getHeight()-cutY; j++)
			{
				alpha = (img.getRGB(0, j) >> 24) & 0xFF ;
				isCurrentPixelBlack = (alpha < 14) ? false : (alpha > 241) ? true : false ;
				
				if(isCurrentPixelBlack && isPrevPixelBlack)
				{
					for(int jj=j; jj<img.getHeight(); jj++)
					{
						for(int ii=0; ii<img.getWidth(); ii++)
							img.setRGB(ii, jj-1, img.getRGB(ii, jj)) ;
					}
					cutY++ ;
					j-- ;
				}
				isPrevPixelBlack = isCurrentPixelBlack ;
			}
			
			logger.info("Packing by X axis...") ; 
			//Goes on X axis
			BufferedImage newImg = img.getSubimage(0, 0, img.getWidth(), img.getHeight()-cutY) ;
			for(int i=1; i<newImg.getWidth()-cutX; i++)
			{
				alpha = (newImg.getRGB(i, 0) >> 24) & 0xFF ;
				isCurrentPixelBlack = (alpha < 14) ? false : (alpha > 241) ? true : false ;
				
				if(isCurrentPixelBlack && isPrevPixelBlack)
				{
					for(int ii=i; ii<newImg.getWidth(); ii++)
					{
						for(int jj=0; jj<newImg.getHeight(); jj++)
							newImg.setRGB(ii-1, jj, newImg.getRGB(ii, jj)) ;
					}
					cutX++ ;
					i-- ;
				}
				isPrevPixelBlack = isCurrentPixelBlack ;
			}
			
			newImg = newImg.getSubimage(0, 0, newImg.getWidth()-cutX, newImg.getHeight()) ;
			
			logger.info("New image size [" +newImg.getWidth()+ " x " +newImg.getHeight()+ "] / " 
					+newImg.getWidth()*newImg.getHeight()+ " pixels.") ;
			logger.info("Image reduced on " +(cutX*img.getHeight() + cutY*img.getWidth() - cutX*cutY)) ;
			
			
			File outputfile = new File("D:\\updatedImg.9.png") ;
		    ImageIO.write(newImg, "png", outputfile) ;

		    logger.info("Image saved like \"" +outputfile.getName()+ "\"") ;
		    logger.info("NinePatchPacker: DONE!") ;	
		}		catch(IOException e)
		{
			throw e ;
		}
	}
	
	
	private static Vertex2D[][] readImage(BufferedImage img)
	{
		int prevPixel = 16777215 ;
		int currentPixel ;
		boolean isFirstPixelBlack = false ;
		
		ArrayList<Integer> horizontalVertices = new ArrayList<Integer>() ;
		ArrayList<Integer> verticalVertices = new ArrayList<Integer>() ;
		
		// IF first pixel [0,0] is black?
		if(img.getRGB(0, 0) == -16777216)
		{
			isFirstPixelBlack = true ;
			prevPixel = -16777216 ;
		}
		
		horizontalVertices.add(0) ;
		verticalVertices.add(0) ;
			
		for(int i=1; i<img.getWidth()-1; i++)
		{
			currentPixel = img.getRGB(i, 0) ;
			if(currentPixel != prevPixel)
			{
				prevPixel = currentPixel ;
				if(currentPixel == -16777216)
				{
					horizontalVertices.add(i) ;
				}
				else
					if(currentPixel == 16777215)
						horizontalVertices.add(i-1) ;
				
			}
		}
		horizontalVertices.add(img.getWidth()-1) ;
		//
		prevPixel = 16777215 ;
		for(int j=1; j<img.getHeight()-1; j++)
		{
			currentPixel = img.getRGB(0, j) ;
			if(currentPixel != prevPixel)
			{
				prevPixel = currentPixel ;
				if(currentPixel == -16777216)
				{
					verticalVertices.add(j) ;
				}
				else
					if(currentPixel == 16777215)
						verticalVertices.add(j-1) ;
			}
		}
		verticalVertices.add(img.getHeight()-1) ;
		
		Vertex2D[][] textureCoords = new Vertex2D[horizontalVertices.size()][verticalVertices.size()] ;
		
		for(int i=0; i<horizontalVertices.size(); i++)
		{
			for(int j=0; j<verticalVertices.size(); j++)
			{
				textureCoords[i][j] = new Vertex2D(horizontalVertices.get(i), verticalVertices.get(j)) ;
			}
		}
		
		if(isFirstPixelBlack)
			textureCoords[0][0].x = -1 ;
		
		return textureCoords ;
	}
	
	
}
